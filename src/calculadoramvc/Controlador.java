/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadoramvc;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author Wicho
 */
public class Controlador implements ActionListener{
    
    
    private Calculadora modelo;
    private CalculadoraFRM vista;

    public Controlador(Calculadora m, CalculadoraFRM v) {
        this.modelo = m;
        this.vista = v;
    }
    
    public void iniciar(){
    
        vista.setTitle("Calculadora");        //Título de la ventana.
        vista.setLocationRelativeTo(null);  //Ubicar al centro de la pantalla.
        vista.setVisible(true);             //Mostrar.
        
        asignarControl();
    
    }
    
    public void asignarControl(){
    
        vista.getCerrar().addActionListener(this);
        vista.getDivision().addActionListener(this);
        vista.getMultiplicacion().addActionListener(this);
        vista.getSuma().addActionListener(this);
        vista.getResta().addActionListener(this);
        vista.getLimpiar().addActionListener(this);
    
    }
    
    private void Sumita(){
    float num1 = Float.parseFloat(
                        vista.getValor1().getText()
        );
    float num2 = Float.parseFloat(
                        vista.getValor2().getText()
        );
    
        modelo.setNum1(num1);
        modelo.setNum2(num2);
        
        vista.getTotal().setText("Total= " + modelo.Sumar());
    
    }
    private void Restita(){
    float num1 = Float.parseFloat(
                        vista.getValor1().getText()
        );
    float num2 = Float.parseFloat(
                        vista.getValor2().getText()
        );
    
        modelo.setNum1(num1);
        modelo.setNum2(num2);
        
        vista.getTotal().setText("Total= " + modelo.Restar());
    }
    private void Multi(){
    float num1 = Float.parseFloat(
                        vista.getValor1().getText()
        );
    float num2 = Float.parseFloat(
                        vista.getValor2().getText()
        );
    
        modelo.setNum1(num1);
        modelo.setNum2(num2);
        
        vista.getTotal().setText("Total= " + modelo.Multi());
    }
    private void Divi(){
    float num1 = Float.parseFloat(
                        vista.getValor1().getText()
        );
    float num2 = Float.parseFloat(
                        vista.getValor2().getText()
        );
    
        modelo.setNum1(num1);
        modelo.setNum2(num2);
        
        vista.getTotal().setText("Total= " + modelo.Dividir());
    }
    
    private void limpiar(){
    
        //Enlazar con las cajas de texto del formulario: vista.
        vista.getValor1().setText("");
        vista.getValor2().setText("");
        
        //Limpiar la etiqueta
        vista.getTotal().setText("Total= ");
        
    }
    
    //Metodo pa irse alv
    private void cerrar(){
    
        System.exit(0);
        
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        
        if(e.getSource() == vista.getSuma()){
            Sumita();
        }
        
        if(e.getSource() == vista.getResta()){
            Restita();
        }
        
        if(e.getSource() == vista.getMultiplicacion()){
            Multi();
        }
        if(e.getSource() == vista.getDivision()){
            Divi();
        }
        
        if(e.getSource() == vista.getLimpiar()){
            limpiar();
        }
        
        if(e.getSource() == vista.getCerrar()){
            cerrar();
        }
        
    }
    
    
    
}
