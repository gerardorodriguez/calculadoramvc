/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadoramvc;

import java.awt.EventQueue;

/**
 *
 * @author Wicho
 */
public class CalculadoraMVC {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
        @Override
        public void run() {
            //Lanzar el controlador
            new Controlador(
                    new Calculadora(),
                    new CalculadoraFRM()
            ).iniciar();
        }
    });
    }
    
}
